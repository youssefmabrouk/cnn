"""
Created on Sat Jan 11 13:06:51 2020
@author: Youssef.Mabrouk
"""
import numpy as np
import scipy.sparse

W_in = np.loadtxt(fname = "layer_in")
W_in = np.array(W_in)
W = np.loadtxt(fname = "er_network")
W = np.array(W)

def rnn(signal, signal_scaling, 
                       network_dimension,  
                       network_scaling, 
                       tikonov, 
                       block_dimension, signal_dimension, 
                       discarding_steps, 
                       training_steps, 
                       prediction_steps):
       
    network = network_scaling*W
    layer_in = signal_scaling*W_in
    network = scipy.sparse.csr_matrix(network)             
    echo = np.zeros([training_steps, network_dimension])    
    t = 0
    while t < discarding_steps+training_steps-1:
        if t < discarding_steps:
            echo[0] = np.tanh(layer_in@signal[t]+network@echo[0])        
        else:
            echo[t-discarding_steps+1] = np.tanh(layer_in@signal[t]+\
            network@echo[t-discarding_steps])   
        t+=1
    R = np.block([echo, echo**2])
    layer_out = np.linalg.solve(R.T@R+tikonov*np.eye(2*network_dimension), 
          R.T @ signal[discarding_steps:discarding_steps+training_steps, :]).T
    echo = echo[-1]
   
    signal_prediction = np.zeros([prediction_steps, signal_dimension])
    signal_prediction[0] = layer_out @ np.concatenate((echo, echo**2), axis=0)
    t = 0
    while t < prediction_steps-1:
        echo = np.tanh(layer_in@signal_prediction[t]+network@echo)
        signal_prediction[t+1] = layer_out @ \
        np.concatenate((echo, echo**2), axis=0)
        t+=1  
    signal_test = signal[discarding_steps+training_steps-1: \
                        discarding_steps+training_steps+prediction_steps-1, :]
    return signal_prediction, signal_test